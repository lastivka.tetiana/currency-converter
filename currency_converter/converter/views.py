import requests
from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from converter.serializers import ConverterSerializer
from currency_converter.settings import API_CONVERTER_URL, API_KEY


class ConverterView(APIView):
    permission_classes = (AllowAny,)

    def post(self, request, format=None):
        # Add serializer validation
        serializer = ConverterSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        validate_data = serializer.validated_data

        # Check currency and convert to equality
        if validate_data['currency'] == validate_data["convert_to"]:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={'message': 'currency and convert_to equals'})

        # Get rate via API
        response = requests.get(f'{API_CONVERTER_URL}?apikey={API_KEY}&base_currency={validate_data["currency"]}')
        # Parse response
        rate_date = response.json()["data"][validate_data["convert_to"]]
        # Calculate result
        result_converter = validate_data["value"] * rate_date
        # Return response
        return Response(status=status.HTTP_200_OK,
                        data={"Currency": validate_data["convert_to"], "Value": result_converter,
                              "Conversion_rate": rate_date})
