from converter.views import ConverterView
from django.urls import path

urlpatterns = [path('', ConverterView.as_view())]
