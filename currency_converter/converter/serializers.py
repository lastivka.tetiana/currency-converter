from rest_framework import serializers


class ConverterSerializer(serializers.Serializer):
    choices = ['USD', 'EUR', 'GBP', 'UAH']

    currency = serializers.ChoiceField(choices=choices)
    value = serializers.FloatField(min_value=0)
    convert_to = serializers.ChoiceField(choices=choices)